the [gemini protocol](gopher://zaibatsu.circumlunar.space:70/1/~solderpunk/gemini)(only via gopher) was created in 2019.

Gemini is a new internet protocol which:

- Is heavier than gopher
- Is lighter than the web
- Will not replace either
- Strives for maximum power to weight ratio
- Takes user privacy very seriously


we're trying to keep this cool corner of the web alive.

# &#35; user gemini

main gemini page: `gemini://envs.net`<br />

envs.net serves user gemini sites from your `~/public_gemini` directory.

if a file called `index.gmi` exists in the directory you're currently browsing to in gemini, it will get processed and displayed.<br />
see [this example](gopher://zaibatsu.circumlunar.space/0/%7esolderpunk/gemini/docs/spec-spec.txt)(only via gopher) for more information on file types and special chars.

## browse gemini-sites
if you're currently connected from a envs.net shell, you can use the following browser.

- [`av98`](https://tildegit.org/solderpunk/AV-98)
    - `av98 gemini://envs.net`
- [`bombadillo`](https://tildegit.org/sloum/bombadillo)
    - `bombadillo gemini://envs.net`

if you want to access the gemini site in your public_gemini dir, use the following link structure:
&nbsp;&nbsp;`gemini://envs.net/~username`<br />
